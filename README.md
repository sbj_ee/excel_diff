So I inherited a project where people were using Excel workbooks with many
different sheets and data splattered all over.  Not in a form where one
could pull it into a pandas dataframe and deal with it gracefully.

So I'd get one file (file_v1.0.xlsx) and then changes would be made and 
another file would be created (file_v2.0.xlsx) and no indication for the
changes.

So I needed a solution to basically do a diff for every sheet cell-by-cell 
and find all changes. I was not finding anything readily available, so I 
just sat down this A.M. and wrote this excel_diff.py which will now save
me time and my sanity.
