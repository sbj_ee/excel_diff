#!../env/bin/python3

import sys
import openpyxl
from openpyxl import load_workbook
import warnings
warnings.filterwarnings("ignore")


def main(file1, file2):
    try:
        wb1 = load_workbook(file1)
        wb2 = load_workbook(file2)
    except:
        print("could not open one of the files - exiting")
        sys.exit()

    # check to ensure they have the same sheets
    if wb1.sheetnames != wb2.sheetnames:
        print("The two files have different sheets - exiting")
        print(wb1.sheetnames)
        print(wb2.sheetnames)
        sys.exit()

    # if we get here, then they have the same sheets
    for sheet in wb1.sheetnames:
        wb1_current_sheet = wb1[sheet]
        wb2_current_sheet = wb2[sheet]
        ws1 = wb1_current_sheet
        ws2 = wb2_current_sheet
        wb1_max_row = wb1_current_sheet.max_row
        wb1_max_column = wb1_current_sheet.max_column
        wb2_max_row = wb2_current_sheet.max_row
        wb2_max_column = wb2_current_sheet.max_column

        if wb2_max_row > wb1_max_row:
            max_row = wb2_max_row
        else:
            max_row = wb1_max_row

        if wb2_max_column > wb1_max_column:
            max_column = wb2_max_column
        else:
            max_column = wb1_max_column


        for i in range(1, max_row+1):
            for j in range(1, max_column+1):
                try:
                    ws1_cell_obj = ws1.cell(row=i,column=j)
                    ws2_cell_obj = ws2.cell(row=i,column=j)
                except:
                    print(".")
                    pass
                if str(ws1_cell_obj.value) != str(ws2_cell_obj.value):
                    print(str(file1) + "  " + str(ws1.title) + " ==> " + "row=" + str(i) + " col=" + str(j) + "   " + str(ws1_cell_obj.value))
                    print(str(file2) + "  " + str(ws2.title) + " ==> " + "row=" + str(i) + " col=" + str(j) + "   " + str(ws2_cell_obj.value))
                    print("-------------------------------------------------------------------------------------------------------")
                # else:
                #     print("SAME => " + str(ws1.title) + " ==> " + "row=" + str(i) + " col=" + str(j) + "   " + str(ws1_cell_obj.value))
                #     print("SAME => " + str(ws2.title) + " ==> " + "row=" + str(i) + " col=" + str(j) + "   " + str(ws2_cell_obj.value))
                #     print("-------------------------------------------------------------------------------------------------------")



if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage:\nexcel_diff.py <file1> <file2>\n\n")
        sys.exit()

    main(sys.argv[1], sys.argv[2])

    
# Stephen B. Johnson  sbj@sgiab.com
# 09-May-2019
